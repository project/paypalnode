paypalnode_pricecalc = function () {
  var uri = $('#edit-price-autocomplete').val();
  var t = Array();

  $('.paypernode').each(function(i,v) {
    if(isNaN(v.value)) {
      v.value = '"'+v.value+'"';
    }
    t[i] = v.value;
   });

  t.reverse();

  var terms = t.join(",");

  $.get(uri,
   { terms: t.join(",") },
   function(data){
     $('#edit-price').attr('value', data);
   }
  );
}

paypalnode_pricecalcAttach = function () {
  $('.paypernode').change( function() {
    paypalnode_pricecalc();
  } );

  // Initialise the price field with default term price
  paypalnode_pricecalc();
};

if (Drupal.jsEnabled) {
 $(document).ready(paypalnode_pricecalcAttach);
}