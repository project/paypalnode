<?php
/*
 * @name PayPalNode Admin
 * @description Backend administration processes
 * @author Mike Carter <ixis.co.uk/contact>
 *
 */

/*
	paypalnode_settings_form
*/
function paypalnode_settings() {
	$form = array();
	$types = array();
	$vocabs = array();

  $form['paypal'] = array('#type' => 'fieldset', '#title' => t('PayPal Account'), '#description' => t('You should ensure your PayPal account is set up as a business to accept payments in your selected currency.'));

	$form['paypal']['paypalnode_email_account'] = array(
		'#type' => 'textfield',
		'#title' => 'PayPal Account',
		'#description' => 'The email address associated with your PayPal account that will accept the payments.',
		'#default_value' => variable_get('paypalnode_email_account', PAYPALNODE_EMAIL_ACCOUNT),
		'#required' => TRUE
	);

  $form['paypal']['paypalnode_currency'] = array(
   '#type'           => 'select',
   '#title'          => t('Currency'),
   '#default_value'  => variable_get('paypalnode_currency', PAYPALNODE_CURRENCY),
   '#options'        => array('USD'=>'USD', 'GBP'=>'GBP', 'EUR'=>'EUR'),
   '#description' => t('Define the currency the merchant PayPal account accepts.'),
   );

	$form['paypal']['paypalnode_thanks_path'] = array(
		'#type' => 'textfield',
		'#title' => t('Thankyou Page'),
		'#description' => 'The path to a page displaying a thankyou message once the payment has been made. Because the new node will not be published instantly, it is not possible to redirect them to the new content straight away.',
		'#default_value' => variable_get('paypalnode_thanks_path', PAYPALNODE_THANKS_PATH),
		'#required' => TRUE
	);

  $form['paypal']['paypalnode_interstitial'] = array(
   '#type' => 'textarea',
	 '#title' => t('Interstitial Message'),
   '#description' => t('An optional message to display after content submission but before being redirected to PayPal. This could be used to inform the user about what is happening. You can include raw HTML here.'),
   '#rows' => 5,
   '#default_value' => variable_get('paypalnode_interstitial', '')
  );

  $form['content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site Content'),
    '#description' => t('A single !content can be made charagable when used in combination with vocabulary terms configured in the !fee.', array('!content' => l(t('content-type'), 'admin/content/types'), '!fee' => l(t('content Fees'), 'admin/content/fees')))
    );

	$available_types = node_get_types();
	foreach($available_types as $type) {
		$types[$type->type] = $type->name;
	}
	asort($types);
	$form['content']['paypalnode_content_type'] = array(
																					 '#type' => 'select',
																					 '#title' => t('Content Type'),
																					 '#description' => t('The content type you want to charge users for publishing.'),
																					 '#options' => $types,
																					 '#size' => 1,
																					 '#default_value' => variable_get('paypalnode_content_type', PAYPALNODE_CHARGE_TYPE)
																					 );

	$available_vocabs = taxonomy_get_vocabularies();
	foreach($available_vocabs as $vocab) {
    if(!$vocab->tags) {
  		$vocabs[$vocab->vid] = $vocab->name;
    }
	}

	$form['content']['paypalnode_vocabularies'] = array(
										'#type' => 'select',
										'#title' => t('Vocabularies'),
										'#description' => t('The vocabularies which contain terms that have an associated price. NOTE: Free tagging vocabularies cannot be used. You can manage costs for combinations of terms in the !fee section.', array('!fee' => l(t('Content Fees'), 'admin/content/fees'))),
										'#options' => $vocabs,
										'#size' => 10,
										'#multiple' => TRUE,
										'#default_value' => variable_get('paypalnode_vocabularies', PAYPALNODE_CHARGE_VOCABULARIES)
										);

  $form['expiry'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Expiration'),
    '#description' => t('Content can be automatically un-published after a certain lifespan specified in days.')
    );

	$form['expiry']['paypalnode_expiry_days'] = array(
		'#type' => 'textfield',
		'#title' => t('Content expiration time'),
    '#field_suffix' => t('day(s)'),
 		'#description' => t('Time specified as a number of days before the content should be unpublished. %never days means the content will never expire.', array('%never' => t('0'))),
		'#default_value' => variable_get('paypalnode_expiry_days', '0'),
    '#size' => 3,
	);

	$form['expiry']['paypalnode_expiry_notify'] = array(
		'#type' => 'checkbox',
		'#title' => t('Notify author'),
 		'#description' => t('Notify author by email about their content expiration.'),
		'#default_value' => variable_get('paypalnode_expiry_notify', '0'),
	);

  $form['expiry']['paypalnode_expiry_email_subject'] = array(
    '#type' => 'textfield',
		'#title' => t('Notification email subject'),
    '#description' => t(''),
		'#default_value' => variable_get('paypalnode_expiry_email_subject', paypalnode_help('notifyemail/subject')),
  );

  $form['expiry']['paypalnode_expiry_email_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Notification email body'),
    '#rows' => 11,
    '#description' => t('If the notify author option is activated (above) then an email will be sent out to the author when the content is unpublished. Available variables are: !username, !site, !site-url, !site-name, !expirydays, !title, !created.'),
    '#default_value' => variable_get('paypalnode_expiry_email_body', paypalnode_help('notifyemail/body'))
  );

	return system_settings_form($form);
}


/*
 * Token module - used to provide token replacements when writing emails.
 */
function paypalnode_token_values($type, $object = NULL) {
  if ($type == 'PayPal Node') {
    $tokens['expirydays'] = variable_get('paypalnode_expiry_days', 'never');
    $tokens['created']    = format_date($object['created'], 'large');
    $tokens['title']      = $object['title'];
    $tokens['username']   = $object['name'];
    return $tokens;
  }
}


/*
 * Token module - used to provide descriptive help about each available token
 * when writing emails.
 */
function paypalnode_token_list($type = 'all') {
  if ($type == 'PayPal Node') {
    $tokens['PayPal Node']['expirydays'] = t("The expiry date for paid content.");
    $tokens['PayPal Node']['created']    = t("Full date of when the content was created.");
    $tokens['PayPal Node']['title']      = t("Title of the content.");
    $tokens['PayPal Node']['username']   = t('Author name of the content.');
    return $tokens;
  }
}


/*
 * Price Admin System
 */
function paypalnode_fee_settings_form() {
  $form = paypalnode_fee_edit_form();
  return $form;
}


function paypalnode_fee_edit_form($terms = '') {
  $op = t('Add cost');

  // Check if there are any term names passed as an argument for editing
  if(arg(4)) {
    $edit_form = paypalnode_fee_select(arg(4));
    $op = t('Update item');
  }

  $form = array();

  $vocabs = variable_get('paypalnode_vocabularies', PAYPALNODE_CHARGE_VOCABULARIES);
  $form['vocab'] = array('#type' => 'fieldset',
                         '#collapsible' => FALSE);

  // List the form elements in reverse order to match Drupals taxonomy form display
  rsort($vocabs);

  foreach($vocabs as $vid) {
    $vocab = taxonomy_get_vocabulary($vid);
    $form['vocab'][$vid] = array(
      '#type' => 'textfield',
      '#title' => $vocab->name,
      '#autocomplete_path' => 'taxonomy/autocomplete/'. $vocab->vid,
      '#maxsize' => 64,
      '#size' => 24,
      '#default_value' => is_array($edit_form['terms'][$vocab->vid]) ? current($edit_form['terms'][$vocab->vid]) : '',
      '#weight' => $vocab->weight,
    );
  }

  $form['price'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 10,
    '#default_value' => number_format($edit_form['price'], 2)
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $op,
    '#weight' => 5
  );

  $form['#tree'] = TRUE;

  return $form;
}


function paypalnode_fee_edit_form_submit($form_id, $form_values) {
  paypalnode_fee_settings_form_submit($form_id, $form_values);

  return 'admin/content/fees';
}


function paypalnode_fee_settings_form_validate($form_id, $form_values) {
  // check the terms exist
}


/*
 * Clean up the submitted cost entry and save to the database
 */
function paypalnode_fee_settings_form_submit($form_id, $form_values) {
  $terms = array();

  foreach($form_values['vocab'] as $term) {
    $terms[] = '"'.$term.'"';
  }

  paypalnode_fee_insert($terms, $form_values['price']);
}


/*
	Add or Update a cost entry in the database
*/
function paypalnode_fee_insert($terms, $price) {
  $tids = implode(',', paypalnode_terms_to_tids($terms));

  // Attempt to update an existing entry, if any
  db_query("UPDATE {term_price} SET price = %f WHERE tids = '%s'", $price, $tids);

  // If there was no existing entry, insert a fresh one
  if(db_affected_rows($result) == 0) {
    $result = db_query("INSERT INTO {term_price} VALUES ('%s', %f)", $tids, $price);
  }
}


/*
	paypalnode_fee_select
*/
function paypalnode_fee_select($tids) {
  $entry = array();

  $term_args = explode(',' , $tids);
  if(count($term_args) > 0) {

    // Convert term names in to term ids
    $terms = array();
    foreach($term_args as $term_name) {
      $term = taxonomy_get_term_by_name($term_name);
      $terms[] = $term[0]->tid;
      $entry['terms'][$term[0]->vid][$term[0]->tid] = $term[0]->name;
    }

    // Generate a string suitable for accessing the database
    $tids = implode(',', $terms);
  }

  // Retrieve matching cost & merge with existing data
  $price = db_fetch_array(db_query("SELECT * FROM {term_price} WHERE tids = '%s'", $tids));

  if(is_array($price)) {
    $entry += $price;
  }

  return $entry;
}


/*
	Delete a cost from the database
*/
function paypalnode_fee_delete($terms) {
  $tids = implode(',', paypalnode_terms_to_tids($terms));
  $r = db_query("DELETE FROM {term_price} WHERE tids = '%s'", $tids);
  return db_affected_rows($r);
}


function paypalnode_fee_delete_form() {
  $terms = arg(4);

  // If no terms provided then redirect
  if(!$terms) {
    drupal_set_message(t('No entry was specified to delete.'), 'error');
    drupal_goto('admin/content/fees');
  }

  $form['tids'] = array('#type' => 'value', '#value' => $terms);
  return confirm_form($form,
    t('Deletion of %title', array('%title' => $terms)),
    'admin/content/fees',
    t('Are you sure you want to delete this cost entry? This action cannot be undone.'),
    t('Delete'), t('Cancel'));
}


function paypalnode_fee_delete_form_submit($form_id, $form_values) {
  if($form_values['op'] == t('Delete') && $form_values['confirm'] == 1) {
		$term_string = '"' . str_replace(',', '","', $form_values['tids']) . '"';
		$terms = explode(',', $term_string);

		if(paypalnode_fee_delete($terms)) {
			drupal_set_message('Entry deleted');
		} else {
			drupal_set_message('Deletion failed.', 'error');
		}
	}

  return 'admin/content/fees';
}


/*
	Produce a list of all the current taxonomy combinations and prices
	USed for the theme table display
*/
function paypalnode_fee_list() {
  $i = 0;
  $costs = array();

  $prices = db_query("SELECT * FROM {term_price} WHERE price != 0 ORDER BY tids");
  while($row = db_fetch_array($prices)) {
    $tids = explode(',', $row['tids']);

    foreach($tids as $term_id) {
      $term = taxonomy_get_term($term_id);
      $costs[$i]['terms'][$term->vid] = $term->name;
    }

    $costs[$i]['price'] = number_format($row['price'], 2);

    $i++;
  }

  return $costs;
}




function theme_paypalnode_fee_settings_form($form) {

  $header = array(t('Categories'), t('Cost'), array('data' => t('Operations'), 'colspan' => 2));
  $rows[] = array(drupal_render($form['vocab']), drupal_render($form['price']), array('data' => drupal_render($form['submit']), colspan => 2));

  foreach (paypalnode_fee_list() as $rid => $price) {
    $terms = implode(', ', $price['terms']);
    $rows[] = array($terms, $price['price'],
                    l(t('edit'), 'admin/content/fees/edit/'. $terms),
                    l(t('delete'), 'admin/content/fees/delete/'. $terms)
                    );
  }

  $output = drupal_render($form);
  $output .= theme('table', $header, $rows, array('width' => '100%'));

  return $output;
}


function theme_paypalnode_fee_edit_form($form) {
  $header = array(t('Categories'), t('Cost'), array('data' => t('Operations'), 'colspan' => 2));
  $rows[] = array(drupal_render($form['vocab']), drupal_render($form['price']), array('data' => drupal_render($form['submit']), colspan => 2));
  $output = drupal_render($form);
  $output .= theme('table', $header, $rows, array('width' => '100%'));

  return $output;
}